<?php
/**
 * Chargement du plugin aoto
 *
 * @plugin     aoto
 * @copyright  2021
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\Aoto\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS['debut_intertitre'] = "\n<h2 class=\"spip\">\n";
$GLOBALS['fin_intertitre'] = "\n</h2>\n";
