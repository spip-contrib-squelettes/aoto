<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Aoto
 *
 * @plugin     Aoto
 * @copyright  2021-2024
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\Aoto\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/meta');
include_spip('inc/cextras');
include_spip('base/aoto');


/**
 * Fonction d'installation et de mise à jour du plugin Aoto.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function aoto_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	cextras_api_upgrade(aoto_declarer_champs_extras(), $maj['create']);
	$maj['1.1.0'] = array(
		array('aoto_desactiver_mediabox'),
	);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin aoto
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function aoto_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(aoto_declarer_champs_extras());
	effacer_config('aoto');
	effacer_meta('mediabox');
	effacer_meta($nom_meta_base_version);
}


/**
 * Fonction pour désactiver mediabox
 *
 * @return void
**/
function aoto_desactiver_mediabox() {
	effacer_meta('mediabox');
	$config['active'] = 'non';
	ecrire_meta('mediabox', serialize($config));
}

