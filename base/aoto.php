<?php
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function aoto_declarer_champs_extras($champs = array()) {

	// Table spip_rubriques
	$champs['spip_rubriques']['aoto_type_rubrique'] = array(
		'saisie' => 'radio',
		'options' => array(
			'nom' => 'aoto_type_rubrique',
			'label' => _T('aoto:aoto_type_rubrique'),
			'sql' => "varchar(30) NOT NULL DEFAULT ''",
			'defaut' => '',
			'data' => array(
				'' => _T('aoto:aoto_type_rubrique_classique'),
				'portfolio' => _T('aoto:aoto_type_rubrique_portfolio'),
				'news' => _T('aoto:aoto_type_rubrique_news'),
				'cc' => _T('aoto:aoto_type_rubrique_cc'),
			),
			'restrictions'=>array('voir' => array('auteur' => ''),	//Tout le monde peut voir
							'modifier' => array('auteur' => 'webmestre')),	//Seuls les webmestres peuvent modifier
		),
	);

	$champs['spip_rubriques']['aoto_menu_cache'] = array(
		'saisie' => 'case',
		'options' => array(
			'nom' => 'aoto_menu_cache',
			'label' => _T('aoto:aoto_menu_cache'),
			'sql' => "varchar(3) NOT NULL DEFAULT ''",
			'defaut' => '',
			'label_case' =>  _T('aoto:aoto_menu_cache1'),
			'restrictions'=>array('voir' => array('auteur' => ''),	//Tout le monde peut voir
								'modifier' => array('auteur' => 'webmestre')),	//Seuls les webmestres peuvent modifier
		),
	);

	$champs['spip_rubriques']['aoto_menu_ssmenu'] = array(
		'saisie' => 'case',
		'options' => array(
			'nom' => 'aoto_menu_ssmenu',
			'label' => _T('aoto:aoto_menu_ssmenu'),
			'sql' => "varchar(3) NOT NULL DEFAULT ''",
			'defaut' => '',
			'label_case' =>  _T('aoto:aoto_menu_ssmenu1'),
			'restrictions'=>array('voir' => array('auteur' => ''),	//Tout le monde peut voir
							'modifier' => array('auteur' => 'webmestre')),	//Seuls les webmestres peuvent modifier
		),
	);

	return $champs;
}


