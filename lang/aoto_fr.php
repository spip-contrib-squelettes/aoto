<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// 4
'404_sorry' => 'Désolé, la page que vous cherchez n\'existe plus.',

// A
'aoto_type_rubrique' => 'Type de rubrique',
'aoto_type_rubrique_classique' => '<strong>Classique: </strong>Liste les articles avec le pseudo-logo (avec lien vers l\'article)',
'aoto_type_rubrique_portfolio' => '<strong>Porfolio:</strong> Liste tous les articles sous forme de planche-contact (sans lien vers l\'article)',
'aoto_type_rubrique_news' => '<strong>News: </strong>Liste tous les articles avec logo et texte complet (sans lien vers l\'article)',
'aoto_type_rubrique_cc' => '<strong>Court-circuit: </strong>On renvoie vers le 1<sup>er</sup> article de la rubrique (trié par numéro ou date)',
'aoto_menu_cache' => 'Menu ',
'aoto_menu_cache1' => 'Cacher cette rubrique dans le menu',
'aoto_menu_ssmenu' => 'Sous-menu',
'aoto_menu_ssmenu1' => 'Afficher les articles de cette rubrique en menu déroulant',



// C
'cfg_intro' => 'Cette page vous permet de personnaliser votre site web (éléments de menu, liens vers réseaux ....)',
'cfg_page_demo' => 'Le squelette est livré avec un page démonstration qui permet de tester la mise en page avec des contenus factices :',
'cfg_page_demo_article' => 'Article démo',
'cfg_rezo_facebook' => 'Page Facebook',
'cfg_rezo_instagram' => 'Page Instagram',
'cfg_rezo_explication' => 'Si vous renseignez les adresses suivantes, l\'icône associée s\'ajoute au menu du site (Facultatif)',
'cfg_menu' => 'Menu',
'cfg_titre_parametrages' => 'Paramètres',
'cfg_sommaire' => 'Page d\'accueil',
'cfg_sommaire_explication' => 'La page d\'accueil est composée d\'une image de fond prise aléatoirement dans le stock des images Les images sont alimentées par deux articles du site (l\'un pour les grands écrans, l\'autre pour les smartphones).',
'cfg_sommaire_explication_astuce' => 'Astuce: il est possible de sélectionner des articles non publiés pour éviter de rendre ces articles techniques visible auu public.',
'cfg_sommaire_photos_desktop' => 'Article réserve d\'images ordinateur de bureau',
'cfg_sommaire_photos_desktop_explication' => 'Article contenant la réserve d\'images pour les grands écrans (format horizontal, taille conseillée 2000x3000 pixels)',
'cfg_sommaire_photos_rwd' => 'Article réserve d\'images smartphone',
'cfg_sommaire_photos_rwd_explication' => 'Article contenant la réserve d\'images pour les smartphones (format vertical,taille conseillée 800x450 pixels)',

// F
'flux_rss' => 'Flux RSS',

// H
'haut_page' => 'Haut de page',

// O
'ours' => 'Le saviez-vous ?<br />Le nom du squelette <strong>Aoto</strong> (青砥) vient d\'une ville de la banlieue Nord-Est de Tōkyō. Sa gare est le terminus de la ligne Keisei',


// T
'top_page' => 'haut de page',
'titre_page_configurer_aoto' => 'Configurer le squelette Aoto',




);
