/********************************
 *  GUI: interface utilisateur
 ********************************/

$(document).ready(function(){

	// Lien en spip_out s'ouvre ds une nouvelle fenetre
	$('a.spip_out,a[rel*=external]').attr('target','_blank').attr('rel','external noopener noreferrer');


	// Go to the top
	// http://www.jqueryscript.net/other/Minimal-Back-To-Top-Functionality-with-jQuery-CSS3.html
	$(window).scroll(function(event){
		var scroll = $(window).scrollTop();
		if (scroll >= 800) {
			$(".go-top").addClass("show");
		} else {
			$(".go-top").removeClass("show");
		}
	});

	$('.go-top a').click(function(){
		$('html, body').animate({
			scrollTop: $( $(this).attr('href') ).offset().top
		}, 1000);
	});

	// scroll doux
	$('.smooth-scroll').click (function() {
		var target = $(this).attr('href');
		var hash = "#" + target.substring(target.indexOf('#')+1);
		$('html, body').animate({ scrollTop:$(hash).offset().top 	}, 600);
		return false;
	});


	// article-figure : activez ou non le zoom
	$('.article-figure-unique').click (function() {
			if ($(this).hasClass('article-figure-unique--disabledImageFit')) {
				$(this).removeClass("article-figure-unique--disabledImageFit");
			} else {
				$(this).addClass("article-figure-unique--disabledImageFit");
			}
			return false;
	});

	// fancybox >	la legende modale est le tag figcaption
	// doc.
	// https://fancyapps.com/fancybox/3/docs/#faq
	// https://codepen.io/fancyapps/pen/aaerxw


	$('[data-fancybox="images"]' ).fancybox({
		// tempo slideshow
		slideShow: {
			speed: 4000
		},

		// gestion des legendes
		caption : function( instance, item ) {
			var legende = $(this).parent().find('figcaption').html();
			if (typeof legende !== 'undefined') {
				return legende;
			}
			return "<!--  -->"; // pas de legende
		}
	});

	// bouton play
	// au lieu de declencher sur le bouton, toute la zone est cliquable
	$('.gondole-play-wrapper-js').click (function() {
		$('.planche-item-lien-1').trigger('click');
		$('.fancybox-button--play').trigger('click');
		return false;
	});



});



// WOW: animation in scroll
// https://wowjs.uk/docs.html
new WOW().init();




